﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api1.Controllers
{
    [Route("api/HealthCheck")]
    public class HealthCheckController : Controller
    {
        [Route("Ping"),HttpGet]
        public IActionResult Ping()
        {
            return Ok();
        }
    }
}